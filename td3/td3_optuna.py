"""
NOTE: export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/cuda-11.2/targets/x86_64-linux/lib

bazel run :td3_optuna -- \
  --env=HalfCheetahBulletEnv-v0 \
  --n_jobs=4 \
  --n_trials=1000 \
  --episodes=100 \
  --eval_episodes=4 \
  --sqlite=$(pwd)/optuna.sqlite
"""

# looks like even importing absl logging before optuna does not make optuna
# use absl logging handler...
from absl import logging

import pybullet_envs
import optuna
import tensorflow as tf
import td3
import gym
from absl import app
from absl import flags
import sys
from typing import Optional

import callbacks as callbacks_lib

_ENV = flags.DEFINE_string("env", 'HalfCheetahBulletEnv-v0',
                           "Environment name")
_N_JOBS = flags.DEFINE_integer("n_jobs", 1, "Number of optimization jobs")
_N_TRIALS = flags.DEFINE_integer("n_trials", 100,
                                 "How many Optuna trials to run")
_EPISODES = flags.DEFINE_integer("episodes", 100,
                                 "How many episodes to run per trial")
_EVAL_EPISODES = flags.DEFINE_integer("eval_episodes", 4,
                                      "Num of eval episodes")
_SQLITE = flags.DEFINE_string("sqlite", None, "SQLite db path")
_STUDY_NAME = flags.DEFINE_string("study_name", None, "Study name")


class OptunaCallback(callbacks_lib.Callback):
    """Over the whole training, do `num_evals` test runs, each over
    `num_episodes` episodes. Test runs are done without exploration.

    TODO: rename num_episodes, currently one name for two concepts
    """
    def __init__(self, env, trial, num_evals, num_episodes):
        self._env = env
        self._trial = trial
        self._num_evals = num_evals
        self._num_episodes = num_episodes

    def on_train_start(self, *, num_episodes: Optional[int]):
        assert num_episodes
        self._frequency = num_episodes // self._num_evals

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        if episode_id == 0 or episode_id % self._frequency != 0:
            return

        loss = self.get_loss(agent)
        # logging.info("Trial %d: loss at episode %d is %.2f",
        #              self._trial.number, episode_id, loss)
        self._trial.report(loss, episode_id)

        if self._trial.should_prune():
            raise optuna.TrialPruned()

    def get_loss(self, agent):
        rewards = []
        for _ in range(self._num_episodes):
            episode_reward = td3.run_episode(
                self._env,
                agent,
                report_metrics=None,
                step_var=None,
                learn=False,
                evalute=True,
            )
            rewards.append(episode_reward)

        # Optuna minimizes the loss; we want to maximize expected reward.
        loss = -tf.reduce_mean(rewards).numpy()
        tf.debugging.check_numerics(loss, "loss is nan")
        return loss


def loss(trial: optuna.trial.Trial):
    # TODO: pass parameters like # of episodes, # of evals etc. as trial user
    # attributes
    #exploration_rate_min = trial.suggest_float('exploration_rate_min',
    #                                           1e-2,
    #                                           1e0,
    #                                           log=True)
    exploration_rate = trial.suggest_float('exploration_rate', 0.0, 1.0)
    # exploration_rate_decay = trial.suggest_float('exploration_rate_decay', 0.0,
    #                                              1.0)
    #exploration_rate_decay = 0.0
    num_episodes = _EPISODES.value
    #with pyvirtualdisplay.Display(visible=0, size=(800, 600)):
    env = gym.make(_ENV.value)
    #env.seed(_seed)
    optuna_callback = OptunaCallback(
        env=env,
        trial=trial,
        # How many times to eval per trial. TODO: make flag
        num_evals=4,
        num_episodes=_EVAL_EPISODES.value,
    )

    #replay_buffer_size_log2 = trial.suggest_int('replay_buffer_size_log2', 1,
    #                                            14)
    # TODO: suggest
    replay_buffer_size = 200_000
    trial.set_user_attr('replay_buffer_size', replay_buffer_size)

    polyak_rate = trial.suggest_float('polyak_rate', 1e-4, 1e-1, log=True)

    agent = td3.TD3Agent(
        observation_space=env.observation_space,
        action_space=env.action_space,
        discount_rate=0.99,  # TODO: suggest
        #log_weights=False,
        #log_value_histograms=False,
        #log_advantage=False,
        #actor_layer_sizes=[128, 32], # TODO: suggest
        #critic_layer_sizes=[128, 32], # TODO: suggest
        actor_learning_rate=trial.suggest_float('actor_learning_rate',
                                                1e-6,
                                                1e-1,
                                                log=True),
        critic_learning_rate=trial.suggest_float('critic_learning_rate',
                                                 1e-6,
                                                 1e-1,
                                                 log=True),
        action_noise_stddev_schedule=lambda: 0.1,  # TODO: suggest
        actor_update_delay=2,
        actor_polyak_rate=polyak_rate,
        critic_polyak_rate=polyak_rate,
        replay_buffer_size=replay_buffer_size,
        minibatch_size=trial.suggest_categorical(
            'minibatch_size', [1, 4, 16, 64, 256, 1024, 4096]),
    )
    random_action_steps = trial.suggest_categorical('random_action_steps',
                                                    [1, 10, 100, 1000, 10000])

    # TODO: with tf.summary.create_file_writer(log_dir).as_default():

    ddpg.run_episodes(
        env=env,
        agent=agent,
        num_episodes=num_episodes,
        callbacks=[
            optuna_callback,
            callbacks_lib.TrainingTQDMCallback(),
        ],
        report_metrics=None,
        random_action_steps=random_action_steps,
    )

    return optuna_callback.get_loss(agent)


def main(argv):
    logging.info("This is an absl log.")
    # TODO: set Optuna to use absl logging; currently this does both some fancy
    # stdout logging, and absl logging
    #optuna.logging.get_logger("optuna").addHandler(
    #    logging.StreamHandler(sys.stdout))
    study_name = _STUDY_NAME.value
    if not study_name:
        study_name = _ENV.value + f'_{_EPISODES.value}_eval{_EVAL_EPISODES.value}'

    if _SQLITE.value:
        storage_name = 'sqlite:///' + _SQLITE.value
    else:
        storage_name = None

    # sampler and pruner are automatically picked
    study = optuna.create_study(
        study_name=study_name,
        storage=storage_name,
        load_if_exists=True,
    )
    study.optimize(loss, n_trials=_N_TRIALS.value, n_jobs=_N_JOBS.value)


if __name__ == '__main__':
    app.run(main)
