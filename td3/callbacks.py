from tqdm.auto import tqdm
from typing import Sequence, Optional
import pathlib
import datetime
import abc


class Callback(abc.ABC):
    def on_train_start(self):
        pass

    def on_train_end(self):
        pass

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        pass


class TrainingTQDMCallback(Callback):
    def __init__(self):
        self._tqdm = None

    def on_train_start(self):
        self._tqdm = tqdm(total=None, leave=False)

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        message = f"Episode {episode_id}: ∑ {episode_reward:.2f}"
        # Useful for tests:
        # _log.info(message)
        self._tqdm.set_description(message)
        self._tqdm.update(n=1)

    def on_train_end(self):
        self._tqdm.close()


class CallbackList(Callback):
    def __init__(self, callbacks: Sequence[Callback]):
        self._callbacks = callbacks

    # TODO: autogenerate

    def on_train_start(self):
        for callback in self._callbacks:
            callback.on_train_start()

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        for callback in self._callbacks:
            callback.on_episode_end(
                agent=agent,
                episode_id=episode_id,
                episode_reward=episode_reward,
            )

    def on_train_end(self):
        for callback in self._callbacks:
            callback.on_train_end()


class SaveWeightsCallback(Callback):
    # TODO: type annotations; _log and _run are Sacred things
    def __init__(self, freq: datetime.timedelta, log_dir, _log, _run):
        self._freq = freq
        self._log_dir = log_dir
        self._log = _log
        self._run = _run
        self._last_save = None

    def _should_save(self):
        return (self._last_save is None
                or datetime.datetime.now() >= self._last_save + self._freq)

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        if not self._should_save():
            return

        base = pathlib.Path(
            self._log_dir) / 'weights' / f'episode{episode_id:05d}'
        base.mkdir(parents=True)
        for name, model in (
            ('critic', agent.critic),
            ('critic_target', agent.critic_target),
            ('actor', agent.actor),
            ('actor_target', agent.actor_target),
                # TODO: also save optimizer state, replay buffer?
        ):
            path = base / (name + '.h5')
            model.save_weights(path)
            self._run.add_artifact(path)
        self._last_save = datetime.datetime.now()
        self._log.info(f"Weights saved in {base}")


# TODO: rename to mention Sacred
class SaveVideoCallback(Callback):
    def __init__(self, env, _run):
        self._env = env
        self._run = _run
        self._already_added_artifacts = set()

    def _maybe_add_artifact(self, path):
        if path not in self._already_added_artifacts:
            self._run.add_artifact(path)
            self._already_added_artifacts.add(path)

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        # Upload new videos as Sacred artifacts.
        if self._env.video_callable(episode_id):
            for video_path, metadata_path in self._env.videos:
                self._maybe_add_artifact(video_path)
                self._maybe_add_artifact(metadata_path)


class CollectEpisodeRewardsCallback(Callback):
    def __init__(self):
        self.episode_rewards = []

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        self.episode_rewards.append(episode_reward)
