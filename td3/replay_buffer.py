import tensorflow as tf


class ReplayBuffer:

    def __init__(self, *, action_space, observation_space, capacity, rng):
        self._capacity = tf.constant(capacity, dtype=tf.int64)
        self._count = tf.Variable(0, dtype=tf.int64, name='replay_count')
        self._prestates = tf.Variable(tf.zeros(
            (capacity, ) + observation_space.shape,
            dtype=observation_space.dtype),
                                      name='replay_prestates')
        self._poststates = tf.Variable(tf.zeros(
            (capacity, ) + observation_space.shape,
            dtype=observation_space.dtype),
                                       name='replay_poststates')
        self._not_done = tf.Variable(tf.zeros((capacity, ), dtype=tf.float32),
                                     name='replay_not_done')
        self._rewards = tf.Variable(tf.zeros((capacity, ), dtype=tf.float32),
                                    name='replay_rewards')
        self._actions = tf.Variable(tf.zeros((capacity, ) + action_space.shape,
                                             dtype=action_space.dtype),
                                    name='replay_actions')
        self._rng = rng

    @tf.function
    def store(self, *, prestate, action, poststate, reward, not_done):
        i = self._count % self._capacity
        self._prestates[i].assign(prestate)
        self._poststates[i].assign(poststate)
        self._actions[i].assign(action)
        self._rewards[i].assign(reward)
        self._not_done[i].assign(not_done)
        self._count.assign_add(1)

    @tf.function
    def sample(self, size):
        minibatch_indices = self._rng.uniform(
            shape=(size, ),
            minval=0,
            maxval=tf.minimum(self._count, self._capacity),
            dtype=tf.int64,
        )
        # 2021-10-11 10:38: looks good
        # tf.print("minibatch_indices", minibatch_indices)
        prestates = tf.gather(self._prestates, minibatch_indices, axis=0)
        actions = tf.gather(self._actions, minibatch_indices, axis=0)
        rewards = tf.gather(self._rewards, minibatch_indices, axis=0)
        poststates = tf.gather(self._poststates, minibatch_indices, axis=0)
        not_done = tf.gather(self._not_done, minibatch_indices, axis=0)
        return prestates, actions, poststates, rewards, not_done
