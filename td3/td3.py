import numpy as np
import tensorflow as tf
from gym.wrappers import monitor
import pathlib
import sacred
import datetime
import gym
from tqdm.auto import tqdm

from tensorboard import manager

from typing import Optional, Sequence

import callbacks as callbacks_lib
import replay_buffer as replay_buffer_lib

tfkl = tf.keras.layers

# Ideas:
#   - Prioritized replay?
#   - Anneal the noise size?


def _mse(x, y):
    return tf.reduce_mean(tf.math.squared_difference(x, y))


def make_actor(
    state_space,
    action_space,
    *,
    layer_sizes,
):
    assert np.array_equal(action_space.low, -action_space.high)
    state_input = tfkl.Input(shape=state_space.shape)
    x = state_input
    for layer_size in layer_sizes:
        x = tfkl.Dense(layer_size, activation='relu')(x)
    assert len(action_space.shape) == 1
    action = tfkl.Dense(action_space.shape[0], activation='sigmoid')(x)

    # action is currently 0..1. rescale it to bounds of action space.
    @tf.function
    def scale_action(action):
        return (action *
                (action_space.high - action_space.low)) + action_space.low

    action = tfkl.Lambda(scale_action)(action)

    return tf.keras.Model(inputs=state_input, outputs=action)


def make_critic(
    state_space,
    action_space,
    *,
    layer_sizes,
):
    state_input = tfkl.Input(shape=state_space.shape)
    action_input = tfkl.Input(shape=action_space.shape)

    sa = tfkl.concatenate([state_input, action_input])

    # Make 2 critics
    critic_estimates = []
    for i in range(2):
        x = sa
        for j, layer_size in enumerate(layer_sizes):
            x = tfkl.Dense(
                layer_size,
                activation='relu',
                name=f'q{i+1}_dense{j+1}',
            )(x)
        critic_estimates.append(
            tfkl.Dense(
                1,
                activation=None,
                name=f'q{i+1}_action_value',
            )(x))

    return tf.keras.Model(inputs=(state_input, action_input),
                          outputs=critic_estimates)


def _clip_actions(actions, action_space):
    """actions: either one action, or tensor of actions"""
    return tf.clip_by_value(actions, action_space.low, action_space.high)


def _polyak_average(*, update, towards, rate):
    for towards_var, update_var in zip(towards.trainable_variables,
                                       update.trainable_variables):
        update_var.assign(rate * towards_var + (1 - rate) * update_var)


class TD3(object):

    def __init__(
        self,
        *,
        observation_space,
        action_space,
        critic_learning_rate,
        actor_learning_rate,
        adam_epsilon,
        critic_layer_sizes,
        policy_layer_sizes,
        rng,
        discount,
        tau,
        policy_noise,
        noise_clip,
        policy_freq,
    ):
        assert len(observation_space.shape) == 1
        assert len(action_space.shape) == 1

        self._observation_space = observation_space
        self._action_space = action_space
        self._rng = rng

        self.actor = make_actor(observation_space,
                                action_space,
                                layer_sizes=policy_layer_sizes)
        actor_weights = self.actor.get_weights()
        assert len(actor_weights) > 0
        self.actor_target = make_actor(observation_space,
                                       action_space,
                                       layer_sizes=policy_layer_sizes)
        self.actor_target.set_weights(actor_weights)
        self.actor_optimizer = tf.keras.optimizers.Adam(
            learning_rate=actor_learning_rate, epsilon=adam_epsilon)

        self.critic = make_critic(observation_space,
                                  action_space,
                                  layer_sizes=critic_layer_sizes)
        critic_weights = self.critic.get_weights()
        assert len(critic_weights) > 0
        self.critic_target = make_critic(observation_space,
                                         action_space,
                                         layer_sizes=critic_layer_sizes)
        self.critic_target.set_weights(critic_weights)
        self.critic_optimizer = tf.keras.optimizers.Adam(
            learning_rate=critic_learning_rate, epsilon=adam_epsilon)

        self.discount = discount
        self.tau = tau
        self.policy_noise = policy_noise
        self.noise_clip = noise_clip
        self.policy_freq = policy_freq

        self.total_it = 0

    def select_action(self, state):
        state = tf.expand_dims(state, axis=0)
        return tf.squeeze(self.actor(state), axis=0).numpy()

    def train(self, replay_buffer, batch_size=256):
        self.total_it += 1

        # Sample replay buffer
        state, action, next_state, reward, not_done = replay_buffer.sample(
            batch_size)

        # Update optimized critics.
        metrics = self._update_critics(state, action, next_state, reward,
                                       not_done)

        # Delayed policy updates
        if self.total_it % self.policy_freq == 0:
            metrics.update(self._update_policy(state))
        return metrics

    @tf.function
    def _update_critics(self, state, action, next_state, reward, not_done):
        # Select action according to policy and add clipped noise
        noise = tf.clip_by_value(
            self._rng.normal(shape=action.shape) * self.policy_noise,
            -self.noise_clip, self.noise_clip)

        next_action = _clip_actions(
            self.actor_target(next_state) + noise, self._action_space)

        # Compute the target Q value
        target_Q1, target_Q2 = self.critic_target((next_state, next_action))
        target_Q1 = tf.squeeze(target_Q1, axis=1)
        target_Q2 = tf.squeeze(target_Q2, axis=1)
        target_Q = tf.math.minimum(target_Q1, target_Q2)
        target_Q = reward + not_done * self.discount * target_Q

        with tf.GradientTape() as tape:
            # Get current Q estimates
            current_Q1, current_Q2 = self.critic((state, action))
            current_Q1 = tf.squeeze(current_Q1, axis=1)
            current_Q2 = tf.squeeze(current_Q2, axis=1)
            tf.debugging.assert_shapes((
                (state, ('batch_size', self._observation_space.shape[0])),
                (action, ('batch_size', self._action_space.shape[0])),
                (next_state, ('batch_size', self._observation_space.shape[0])),
                (reward, ('batch_size', )),
                (not_done, ('batch_size', )),
                (target_Q, ('batch_size', )),
                (current_Q1, ('batch_size', )),
                (current_Q2, ('batch_size', )),
            ))

            # Compute critic loss
            critic_loss = (_mse(current_Q1, target_Q) +
                           _mse(current_Q2, target_Q))

        # Optimize the critic
        self.critic_optimizer.minimize(
            critic_loss, tape=tape, var_list=self.critic.trainable_variables)
        return {'critic_loss': critic_loss}

    @tf.function
    def _update_policy(self, state):
        # Compute actor loss
        with tf.GradientTape() as tape:
            actor_loss = -tf.reduce_mean(
                self.critic((state, self.actor(state)))[0])

        # Optimize the actor
        self.actor_optimizer.minimize(actor_loss,
                                      tape=tape,
                                      var_list=self.actor.trainable_variables)

        # Update the frozen target models
        _polyak_average(update=self.critic_target,
                        towards=self.critic,
                        rate=self.tau)
        _polyak_average(update=self.actor_target,
                        towards=self.actor,
                        rate=self.tau)
        return {'actor_loss': actor_loss}


def eval_policy(
    agent,
    env_name,
    *,
    eval_episodes,
):
    eval_env = gym.make(env_name)
    #if do_render_env:
    #    eval_env.render()
    eval_env.seed(54321)

    episode_rewards = []
    for _ in tqdm(range(eval_episodes), leave=False):
        episode_reward = 0
        state, done = eval_env.reset(), False
        while not done:
            action = agent.select_action(state)
            state, reward, done, _info = eval_env.step(action)
            episode_reward += reward
        episode_rewards.append(episode_reward)

    return episode_rewards


def run_episodes(
    *,
    env,
    agent,
    callbacks,
    env_name: str,
    replay_buffer_capacity: int,
    random_action_steps: int,
    evaluate_freq: int,
    evaluation_episodes: int,
    exploration_noise: float,
    batch_size: int,
    log_train_step_metrics: bool,
    _log,
    rng,
    report_metrics=None,
):
    """
    Args:
      evaluate_freq: in steps
      evaluation_noise: stddev of noise added to each step
    """
    if report_metrics is None:
        report_metrics = lambda _: None

    step_var = tf.Variable(0, dtype=tf.int64, name='step')
    tf.summary.experimental.set_step(step_var)

    callback = callbacks_lib.CallbackList(callbacks)
    callback.on_train_start()

    replay_buffer = replay_buffer_lib.ReplayBuffer(
        action_space=env.action_space,
        observation_space=env.observation_space,
        capacity=replay_buffer_capacity,
        rng=rng,
    )

    observation = env.reset()
    episode = 0
    episode_reward = 0
    episode_step = 0
    try:
        while True:
            do_random_step = (step_var < random_action_steps)
            if log_train_step_metrics:
                report_metrics({'random_step': int(do_random_step)})
            if do_random_step:
                action = env.action_space.sample()
            else:
                action = agent.select_action(observation)
                # Add exploration noise.
                noise = rng.normal(shape=action.shape) * exploration_noise
                if log_train_step_metrics:
                    report_metrics({'exploration_noise_l2': tf.norm(noise)})
                action = _clip_actions(action + noise,
                                       env.action_space).numpy()

            next_observation, reward, done, info = env.step(action)
            episode_reward += reward

            replay_buffer.store(
                prestate=tf.constant(observation,
                                     dtype=env.observation_space.dtype),
                action=tf.constant(action, dtype=env.action_space.dtype),
                poststate=tf.constant(next_observation,
                                      dtype=env.observation_space.dtype),
                reward=tf.constant(reward, dtype=tf.float32),
                not_done=tf.constant(0.0 if done else 1.0, dtype=tf.float32),
            )
            observation = next_observation

            if not do_random_step:
                metrics = agent.train(replay_buffer, batch_size=batch_size)
                if log_train_step_metrics or episode_step == 1:
                    report_metrics(metrics)

            if done:
                observation = env.reset()
                callback.on_episode_end(
                    agent=agent,
                    episode_id=episode,
                    episode_reward=episode_reward,
                )
                report_metrics({'train/episode/reward': episode_reward})
                episode_reward = 0
                episode_step = 0
                episode += 1
                report_metrics({'episode/index': episode})

            episode_step += 1
            step_var.assign_add(1)

            if step_var % evaluate_freq == 0:
                episode_rewards = eval_policy(
                    agent, env_name, eval_episodes=evaluation_episodes)
                mean_reward = tf.reduce_mean(episode_rewards).numpy()
                _log.info(f"Avg reward: {mean_reward:.2f}")
                report_metrics({'eval/episode/mean_reward': mean_reward})
                tf.summary.histogram('eval_episode_reward', episode_rewards)

        # tf.summary.trace_on(graph=True, profiler=True)
        # tf.summary.trace_export(name=f'episode_trace/{episode.read_value()}')

    finally:
        callback.on_train_end()


exp = sacred.Experiment('half-cheetah-td3', interactive=True)


@exp.config
def config():
    environment = 'HalfCheetahBulletEnv-v0'
    log_dir = None
    log_train_step_metrics: bool = None

    critic_learning_rate: float = 1e-3
    actor_learning_rate: float = 1e-3
    discount: float = 0.99
    policy_noise: float = None
    noise_clip: float = None
    log_scalars = True
    exploration_noise: float = None
    record_video = True
    policy_freq: int = None
    replay_buffer_capacity: int = None
    batch_size: int = None
    tau: float = None
    random_action_steps: int = 10000
    critic_layer_sizes = [300, 400]
    policy_layer_sizes = [300, 400]

    save_weights_freq_minutes = 10
    do_render_env: bool = False
    load_models_from = None
    evaluate_freq: int = None
    evaluation_episodes: int = None
    adam_epsilon: float = None


@exp.automain
def run(
    _run,
    _seed,
    _log,
    environment,
    log_dir,
    critic_learning_rate,
    actor_learning_rate,
    discount,
    policy_noise: float,
    noise_clip,
    policy_freq,
    replay_buffer_capacity: int,
    batch_size: int,
    log_scalars: bool,
    do_render_env: bool,
    record_video: bool,
    tau: float,
    random_action_steps: int,
    save_weights_freq_minutes: int,
    exploration_noise: float,
    evaluate_freq: int,
    evaluation_episodes: int,
    load_models_from: Optional[str],
    adam_epsilon: float,
    log_train_step_metrics: bool,
    critic_layer_sizes,
    policy_layer_sizes,
    callbacks: Sequence[callbacks_lib.Callback] = None,
):
    _log.info("***************************************************")
    if log_dir is None:
        log_dir = f'/tmp/{environment}-td3/{datetime.datetime.now().isoformat()}'
        _log.info(f"log_dir not specified, picking {log_dir}")

    # TODO: would be nice to also launch TensorBoard and open it ourselves
    # _log.info(f"To follow along in TensorBoard, run:")
    # _log.info(f"   tensorboard --logdir {log_dir}")
    # _log.info("***************************************************")

    start_result = manager.start(['--logdir', log_dir])
    if isinstance(start_result, manager.StartLaunched):
        _log.info(
            f"TensorBoard running at http://localhost:{start_result.info.port}"
        )
    elif isinstance(start_result, manager.StartReused):
        _log.info(
            f"Reused TensorBoard at http://localhost:{start_result.info.port}. "
            "(Use '!kill {start_result.info.pid}' to kill it.)")
    elif isinstance(start_result, manager.StartFailed):

        def format_stream(name, value):
            if value == "":
                return ""
            elif value is None:
                return "\n<could not read %s>" % name
            else:
                return "\nContents of %s:\n%s" % (name, value.strip())

        _log.error("ERROR: Failed to launch TensorBoard (exited with %d)." % (
            start_result.exit_code,
            format_stream("stderr", start_result.stderr),
            format_stream("stdout", start_result.stdout),
        ))

    elif isinstance(start_result, manager.StartExecFailed):
        _log.error("ERROR: Failed to start Tensorboard: %s" %
                   start_result.os_error)
    elif isinstance(start_result, manager.StartTimedOut):
        _log.error("ERROR: Timed out waiting for TensorBoard to start. "
                   "It may still be running as pid %d." % start_result.pid)
    else:
        raise TypeError("Unexpected result from `manager.start`")

    # TODO: Sacred's LogFileWriter ought to populate this. See
    # https://github.com/IDSIA/sacred/issues/836.
    exp.info['tensorflow'] = {'log_dir': log_dir}

    env = gym.make(environment)
    if do_render_env:
        env.render()
    if record_video:
        env = monitor.Monitor(env, log_dir, force=True)
    env.seed(_seed)
    env.action_space.seed(_seed)

    _log.info("Environment reward threshold is %f." %
              env.spec.reward_threshold)

    if callbacks is None:
        callbacks = [callbacks_lib.TrainingTQDMCallback()]

    # TODO: make this callback independent
    if save_weights_freq_minutes > 0:
        callbacks.append(
            callbacks_lib.SaveWeightsCallback(
                freq=datetime.timedelta(minutes=save_weights_freq_minutes),
                _log=_log,
                _run=_run,
                log_dir=log_dir,
            ))
    if record_video:
        callbacks.append(callbacks_lib.SaveVideoCallback(env=env, _run=_run))

    # TODO: move to callback
    def report_metrics(metrics):
        if not log_scalars:
            return

        for metric, value in metrics.items():
            tf.debugging.assert_scalar(value, f"metric not scalar: {metric}")
            tf.summary.scalar(metric, value)
            if isinstance(value, tf.Tensor):
                value = value.numpy()

            # Sacred metrics reporting:
            #   step = tf.summary.experimental.get_step().read_value().numpy()
            #   _run.log_scalar(
            #       metric,
            #       value,
            #       step=step,
            #   )

    rng = tf.random.Generator.from_seed(_seed)

    agent = TD3(
        adam_epsilon=adam_epsilon,
        observation_space=env.observation_space,
        action_space=env.action_space,
        critic_learning_rate=critic_learning_rate,
        actor_learning_rate=actor_learning_rate,
        policy_noise=policy_noise,
        noise_clip=noise_clip,
        discount=discount,
        policy_freq=policy_freq,
        tau=tau,
        critic_layer_sizes=critic_layer_sizes,
        policy_layer_sizes=policy_layer_sizes,
        rng=rng,
    )
    if load_models_from:
        # TODO: deduplicate
        # TODO: how to save more complex checkpoint in TF?
        for name, model in (
            ('critic', agent.critic),
            ('critic_target', agent.critic_target),
            ('policy', agent.actor),
            ('policy_target', agent.actor_target),
        ):
            path = pathlib.Path(load_models_from) / (name + '.h5')
            model.load_weights(path)
        _log.info(f"Loaded models from {load_models_from}")

    with tf.summary.create_file_writer(log_dir).as_default():
        run_episodes(
            env=env,
            agent=agent,
            callbacks=callbacks,
            env_name=environment,
            report_metrics=report_metrics,
            random_action_steps=random_action_steps,
            evaluate_freq=evaluate_freq,
            exploration_noise=exploration_noise,
            evaluation_episodes=evaluation_episodes,
            replay_buffer_capacity=replay_buffer_capacity,
            log_train_step_metrics=log_train_step_metrics,
            _log=_log,
            rng=rng,
            batch_size=batch_size,
        )


# possible: env.render(close=True)
# (or env.render(mode='close') ?)
