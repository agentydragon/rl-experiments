"""General scaffolding for running an agent in an environment."""

import tensorflow as tf

import itertools
from typing import Optional

import callbacks as callbacks_lib


def run_episode(
    env,
    agent,
    exploration_rate,
    report_metrics=None,
    step_var=None,
    learn=True,
):
    """Runs an agent on a single episode."""
    if report_metrics is None:
        report_metrics = lambda _: None
    observation = env.reset()
    total_reward = 0

    action, metrics = agent.start(observation,
                                  exploration_rate=exploration_rate)
    report_metrics(metrics)
    num_steps = 0

    while True:
        observation, reward, done, info = env.step(action)
        total_reward += reward

        report_metrics({
            'episode/exploration_rate': exploration_rate,
        })

        if step_var:
            step_var.assign_add(1)
        num_steps += 1

        if done:
            report_metrics(agent.finish(reward, exploration_rate, learn=learn))
            break

        #do_trace_step = (episode == 10)
        #if do_trace_step:
        #    tf.summary.trace_on(graph=True, profiler=True)
        action, metrics = agent.step(
            reward,
            observation,
            exploration_rate=exploration_rate,
            learn=learn,
        )
        action = action.numpy()
        #if do_trace_step:
        #    tf.summary.trace_export(name='episode_step_trace')
        report_metrics(metrics)

    # 3rd parameter is step count
    report_metrics({
        'episode/step_count': num_steps,
        'episode/total_reward': total_reward,
    })
    return total_reward


def run_episodes(
    env,
    agent,
    num_episodes: Optional[int],
    callbacks,
    # TODO: make callback
    exploration_rate_schedule,
    report_metrics=None,
    learn=True,
):
    if report_metrics is None:
        report_metrics = lambda _: None
    step_var = tf.Variable(0, dtype=tf.int64, name='step')
    tf.summary.experimental.set_step(step_var)

    if num_episodes:
        episodes = range(num_episodes)
    else:
        episodes = itertools.count()

    callback = callbacks_lib.CallbackList(callbacks)
    callback.on_train_start(num_episodes=num_episodes)
    try:
        for episode in episodes:
            report_metrics({'episode/index': episode})
            exploration_rate = exploration_rate_schedule(episode)
            total_reward = run_episode(
                env,
                agent,
                exploration_rate,
                report_metrics,
                step_var,
                learn=learn,
            )

            # TODO: Save and upload current model.
            callback.on_episode_end(
                agent=agent,
                episode_id=episode,
                episode_reward=total_reward,
            )
    finally:
        callback.on_train_end()
