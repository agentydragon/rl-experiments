"""
bazel run :lunar_lander_ac_optuna -- \
  --n_trials=1000 \
  --episodes=100 \
  --sqlite=$(pwd)/optuna.sqlite
"""

# looks like even importing absl logging before optuna does not make optuna
# use absl logging handler...
from absl import logging

import optuna
import tensorflow as tf
import lunar_lander_ac
import gym
from absl import app
from absl import flags
import sys
from typing import Optional

import callbacks as callbacks_lib
import scaffold as scaffold_lib

_N_JOBS = flags.DEFINE_integer("n_jobs", 1, "Number of optimization jobs")
_N_TRIALS = flags.DEFINE_integer("n_trials", 100,
                                 "How many Optuna trials to run")
_EPISODES = flags.DEFINE_integer("episodes", 100,
                                 "How many episodes to run per trial")
_EVAL_EPISODES = flags.DEFINE_integer("eval_episodes", 10,
                                      "Num of eval episodes")
_SQLITE = flags.DEFINE_string("sqlite", None, "SQLite db path")
_STUDY_NAME = flags.DEFINE_string("study_name", None, "Study name")


class OptunaCallback(callbacks_lib.Callback):
    """Over the whole training, do `num_evals` test runs, each over
    `num_episodes` episodes. Test runs are done without exploration.

    TODO: rename num_episodes, currently one name for two concepts
    """
    def __init__(self, env, trial, num_evals, num_episodes):
        self._env = env
        self._trial = trial
        self._num_evals = num_evals
        self._num_episodes = num_episodes

    def on_train_start(self, *, num_episodes: Optional[int]):
        assert num_episodes
        self._frequency = num_episodes // self._num_evals

    def on_episode_end(self, *, agent, episode_id, episode_reward):
        if episode_id == 0 or episode_id % self._frequency != 0:
            return

        loss = self.get_loss(agent)
        # logging.info("Trial %d: loss at episode %d is %.2f",
        #              self._trial.number, episode_id, loss)
        self._trial.report(loss, episode_id)

        if self._trial.should_prune():
            raise optuna.TrialPruned()

    def get_loss(self, agent):
        rewards = []
        for _ in range(self._num_episodes):
            episode_reward = scaffold_lib.run_episode(
                self._env,
                agent,
                exploration_rate=0.0,
                report_metrics=None,
                step_var=None,
                learn=False,
            )
            rewards.append(episode_reward)

        # Optuna minimizes the loss; we want to maximize expected reward.
        loss = -tf.reduce_mean(rewards).numpy()
        tf.debugging.check_numerics(loss, "loss is nan")
        return loss


def loss(trial: optuna.trial.Trial):
    # TODO: pass parameters like # of episodes, # of evals etc. as trial user
    # attributes
    #exploration_rate_min = trial.suggest_float('exploration_rate_min',
    #                                           1e-2,
    #                                           1e0,
    #                                           log=True)
    exploration_rate = trial.suggest_float('exploration_rate', 0.0, 1.0)
    # exploration_rate_decay = trial.suggest_float('exploration_rate_decay', 0.0,
    #                                              1.0)
    #exploration_rate_decay = 0.0
    num_episodes = _EPISODES.value
    #with pyvirtualdisplay.Display(visible=0, size=(800, 600)):
    env = gym.make('LunarLander-v2')
    #env.seed(_seed)
    optuna_callback = OptunaCallback(
        env=env,
        trial=trial,
        # How many times to eval per trial. TODO: make flag
        num_evals=4,
        num_episodes=_EVAL_EPISODES.value,
    )

    replay_buffer_size_log2 = trial.suggest_int('replay_buffer_size_log2', 1,
                                                14)
    replay_buffer_size = 2**replay_buffer_size_log2
    trial.set_user_attr('replay_buffer_size', replay_buffer_size)

    agent = lunar_lander_ac.ActorCriticAgent(
        observation_space=env.observation_space,
        action_space=env.action_space,
        discount_rate=1.0,
        log_weights=False,
        log_value_histograms=False,
        log_advantage=False,
        actor_layer_sizes=[128, 32],
        critic_layer_sizes=[128, 32],
        actor_learning_rate=trial.suggest_float('actor_learning_rate',
                                                1e-6,
                                                1e-1,
                                                log=True),
        critic_learning_rate=trial.suggest_float('critic_learning_rate',
                                                 1e-6,
                                                 1e-1,
                                                 log=True),
        replay_buffer_size=replay_buffer_size,
        critic_sync_probability=trial.suggest_float('critic_sync_probability',
                                                    1e-6,
                                                    1e-1,
                                                    log=True),
    )

    # TODO: with tf.summary.create_file_writer(log_dir).as_default():

    scaffold_lib.run_episodes(
        env=env,
        agent=agent,
        num_episodes=num_episodes,
        callbacks=[optuna_callback,
                   callbacks_lib.TrainingTQDMCallback()],
        # TODO: factor exploration_rate_schedule together into one function
        #exploration_rate_schedule=lambda episode:
        #(exploration_rate_min + (1.0 - exploration_rate_min) *
        # (exploration_rate_decay**(episode + 1))),
        exploration_rate_schedule=lambda _episode: exploration_rate,
        report_metrics=None,
    )

    return optuna_callback.get_loss(agent)


def main(argv):
    logging.info("This is an absl log.")
    # TODO: set Optuna to use absl logging; currently this does both some fancy
    # stdout logging, and absl logging
    #optuna.logging.get_logger("optuna").addHandler(
    #    logging.StreamHandler(sys.stdout))
    study_name = _STUDY_NAME.value
    if not study_name:
        study_name = f'lunar_lander_ac_ep{_EPISODES.value}_eval{_EVAL_EPISODES.value}'

    if _SQLITE.value:
        storage_name = 'sqlite:///' + _SQLITE.value
    else:
        storage_name = None

    # sampler and pruner are automatically picked
    study = optuna.create_study(
        study_name=study_name,
        storage=storage_name,
        load_if_exists=True,
    )
    study.optimize(loss, n_trials=_N_TRIALS.value, n_jobs=_N_JOBS.value)


if __name__ == '__main__':
    app.run(main)
