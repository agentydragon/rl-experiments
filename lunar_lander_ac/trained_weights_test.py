import os.path
import datetime

import gym
from gym.wrappers import monitor
from absl import logging
from absl.testing import absltest
import pyvirtualdisplay
from rules_python.python.runfiles import runfiles

import callbacks as callbacks_lib
import scaffold as scaffold_lib
import lunar_lander_ac

# This test is detached from the smoke test because of this segfault which
# I got otherwise:
#
#   0  __GI___pthread_mutex_lock (mutex=0x8) at ../nptl/pthread_mutex_lock.c:67
#   1  0x00007ffe9c078ad6 in  () at /lib/x86_64-linux-gnu/libEGL_mesa.so.0
#   2  0x00007ffe9c0acf63 in eglReleaseThread () at /lib/x86_64-linux-gnu/libEGL.so.1
#   3  0x00007ffe9c358a8c in  () at /lib/x86_64-linux-gnu/libGLX_nvidia.so.0
#   4  0x00007ffe9c357d74 in  () at /lib/x86_64-linux-gnu/libGLX_nvidia.so.0
#   5  0x00007ffe9c30fe4f in  () at /lib/x86_64-linux-gnu/libGLX_nvidia.so.0
#   6  0x00007ffff7ffd040 in _rtld_global () at /lib64/ld-linux-x86-64.so.2


class SavedWeightsTest(absltest.TestCase):
    def test_saved_model(self):
        """
        Tests that the weights in trained_weights directory have good
        performance.

        Config:
            critic_learning_rate = 1e-4
            actor_layer_sizes = [128, 32]
            critic_layer_sizes = [128, 32]
            actor_learning_rate = 1e-6
            replay_buffer_size = 2**11
            discount_rate = 1.0
            critic_sync_probability = 1e-3
            exploration_rate_min = 0.1
            exploration_rate_decay = 0.0

        Trained model was manually selected on episode 2955.
        """
        with pyvirtualdisplay.Display(visible=0, size=(800, 600)):
            env = gym.make('LunarLander-v2')
            # TODO: put into testdir
            # env = monitor.Monitor(env, '/tmp/lunar_lander_ac_test', force=True)
            env.seed(0)
            # TODO: deterministically seed for action sampling
            agent = lunar_lander_ac.ActorCriticAgent(
                observation_space=env.observation_space,
                action_space=env.action_space,
                actor_layer_sizes=[128, 32],
                critic_layer_sizes=[128, 32],
                actor_learning_rate=0.0,
                critic_learning_rate=0.0,
                replay_buffer_size=1,
                discount_rate=1.0,
                critic_sync_probability=0.0,
                log_weights=False,
                log_value_histograms=False,
                log_advantage=False,
            )
            r = runfiles.Create()
            actor = r.Rlocation(
                'rl_experiments/lunar_lander_ac/trained_weights/actor.h5')
            agent._actor.load_weights(actor)
            episodes = 50

            episode_rewards_cb = callbacks_lib.CollectEpisodeRewardsCallback()

            scaffold_lib.run_episodes(
                env=env,
                agent=agent,
                num_episodes=episodes,
                callbacks=[episode_rewards_cb],
                report_metrics=None,
                exploration_rate_schedule=lambda _episode: 0.0,
            )
            env.close()

            mean = sum(episode_rewards_cb.episode_rewards) / episodes
            # actually got: 194.4475
            # 212.76, 226.73, 235.92, 250.09, 221.89, 247.45, 122.90, 256.25,
            # 254.55, -80.47, 235.98, 261.91, 11.31, 248.51, 268.90, 12.55, 222.18,
            # 231.89, 218.84, 228.81
            # std: 194.4475, so std of sample is ~9.7
            #
            # mean - 3 sigma is approximately 164. 150 should be fine.
            logging.info("Mean reward: %.2f", mean)
            assert mean >= 150.0


if __name__ == '__main__':
    absltest.main()
