# Lunar lander actor-critic

Actor-critic solution to Lunar Lander environment. See
https://gym.openai.com/envs/LunarLander-v2/.

TODO: add some stored models, graphs, videos
