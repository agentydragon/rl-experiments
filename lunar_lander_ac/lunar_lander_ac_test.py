import os.path
import datetime

import gym
from gym.wrappers import monitor
import tensorflow as tf
from absl import logging
from absl.testing import absltest
import pyvirtualdisplay

import lunar_lander_ac


class LunarLanderACTest(absltest.TestCase):

    def test_smoke(self):
        with pyvirtualdisplay.Display(visible=0, size=(800, 600)):
            # TODO: run as an experiment
            # TODO: maybe test that expected value actually improves, i.e. that
            # training actually works?
            lunar_lander_ac.exp.run(
                config_updates={
                    'num_episodes': 10,
                    'actor_layer_sizes': [32],
                    'critic_layer_sizes': [32],
                    'replay_buffer_size': 128,
                    'log_scalars': False,
                    'record_video': False,
                })


if __name__ == '__main__':
    absltest.main()
