"""
bazel run :lunar_lander_ac_main -- --mongo_db=sacred
"""

import sacred
from gym.wrappers import monitor
import gym
import tensorflow as tf
import tensorflow_probability as tfp

import itertools
import datetime
from typing import Sequence, Optional

import callbacks as callbacks_lib
import scaffold as scaffold_lib

tfd = tfp.distributions
tfpl = tfp.layers
tfkl = tf.keras.layers

# TODO: useful histograms are currently only reported to TensorBoard. would
# be nice to also report them into Sacred in some form. like percentiles.


def _make_actor(
    observation_space: gym.spaces.Box,
    action_space: gym.spaces.Discrete,
    layer_sizes: Sequence[int],
):
    r"""Represents a learned parameterized stochastic policy $\pi_\theta$

    The actor's policy is a mixture of a softmax policy (with logits computed
    by a fully connected network), and uniform random exploration.

    We use Tensorflow Probability to build the actual action distribution
    inside the actor Keras model, and to sample from it, compute log
    probabilities, and their gradients.
    """
    observation = tfkl.Input(shape=observation_space.shape)
    exploration_rate = tfkl.Input(shape=())

    x = observation
    for i, layer_size in enumerate(layer_sizes):
        x = tfkl.Dense(layer_size, activation='relu', name=f'relu{i}')(x)

    n = action_space.n
    action_logits = tfkl.Dense(n, activation=None, name='action_logits')(x)

    def make_action_distribution(inputs):
        action_logits, exploration_rate = inputs

        trained_actions = tfd.Categorical(logits=action_logits)

        batch_size = tf.shape(action_logits)[0]
        explored_actions = tfd.Categorical(probs=tf.ones((
            batch_size,
            n,
        )) * (1.0 / n))
        cat = tfd.Categorical(
            probs=tf.stack([1.0 - exploration_rate, exploration_rate], axis=1))
        mixture = tfd.Mixture(cat=cat,
                              components=[trained_actions, explored_actions])
        return mixture

    actions = tfpl.DistributionLambda(make_action_distribution,
                                      name='action_dist')(
                                          (action_logits, exploration_rate))
    return tf.keras.Model(
        inputs={
            'observation': observation,
            'exploration_rate': exploration_rate
        },
        outputs=actions,
    )


def _make_critic(
    observation_space: gym.spaces.Box,
    action_space: gym.spaces.Discrete,
    layer_sizes: Sequence[int],
):
    r"""Approximates $\mathrm{Q}_{\pi_\theta}$ and $\mathrm{V}_{\pi_\theta}$.

    We compute $\mathrm{\hat{V} }_{\pi_\theta}$ first, and then compute
    the action values $\mathrm{\hat{Q} }_{\pi_\theta}$ by adding up the state
    value and residual connection (i.e., the learned advantage of each action).
    """
    observation = tfkl.Input(shape=observation_space.shape)

    x = observation
    for i, layer_size in enumerate(layer_sizes):
        x = tfkl.Dense(layer_size, activation='relu', name=f'relu{i}')(x)

    action_deltas = tfkl.Dense(action_space.n,
                               activation=None,
                               name='action_deltas')(x)
    state_value = tfkl.Dense(1, activation=None, name='state_value')(x)

    # Residual connection.
    action_values = tfkl.Lambda(
        lambda x: x[0] + tf.broadcast_to(x[1],
                                         (tf.shape(x[0])[0], action_space.n)),
        name='action_value_residual')((action_deltas, state_value))

    return tf.keras.Model(
        inputs=observation,
        outputs={
            'action_values': action_values,
            'state_value': state_value,
        },
    )


class ReplayBuffer:
    def __init__(
        self,
        capacity,
        observation_space: gym.spaces.Box,
        action_space: gym.spaces.Discrete,
    ):
        # Observation space should be a 1D box of floats.
        assert observation_space.dtype.name == 'float32'
        assert len(observation_space.shape) == 1

        # Replay buffer: S, A, R, S'
        self._capacity = capacity
        # TODO: TensorArray for potentially unlimited size?
        self._count = tf.Variable(0, dtype=tf.int64, name='replay_count')
        self._prestates = tf.Variable(
            tf.zeros((self._capacity, ) + observation_space.shape,
                     dtype=tf.float32),
            name='prestates',
        )
        self._actions = tf.Variable(
            tf.zeros((self._capacity, ), dtype=tf.int32),
            name='actions',
        )
        self._rewards = tf.Variable(
            tf.zeros((self._capacity, ), dtype=tf.float32),
            name='rewards',
        )
        self._poststates = tf.Variable(
            tf.zeros((self._capacity, ) + observation_space.shape,
                     dtype=tf.float32),
            name='poststates',
        )
        self._done = tf.Variable(
            tf.zeros((self._capacity, ), dtype=tf.bool),
            name='done',
        )
        self._used = tf.Variable(
            tf.zeros((self._capacity, ), dtype=tf.bool),
            name='used',
        )

    @tf.function
    def store(self, prestate, action, reward, poststate, done):
        i = self._count % self._capacity
        self._prestates[i].assign(prestate)
        self._actions[i].assign(action)
        self._rewards[i].assign(reward)
        self._poststates[i].assign(poststate)
        self._done[i].assign(done)
        self._used[i].assign(True)
        self._count.assign_add(1)


class ActorCriticAgent:
    r"""
    The agent keeps 2 critics. One of them is optimized, and the predictions of
    the other are used as targets for the optimized critic (and the actor).
    This is the "double Q learning" trick, and it helps against the critic
    "bootstrapping itself to infinite values".

    TODO(agentydragon): I bet that phenomenon has a well-established name.

    On every step, with probability `critic_sync_probability`, the weights of
    the target critic are set to the weights of the optimized critic.

    We use a replay buffer to train both the critic(s) and the actor.

    For a transition of $(s_1 \rightarrow_{a,r} s_2)$, we update:

    * $\mathrm{\hat{V} }(s_1)$ towards
      $r + \gamma \mathbb{E}_{a'\sim \pi(s_2)}[\mathrm{\hat{Q} }(s_2, a')]$
      (i.e., expected SARSA),
    * $\mathrm{\hat{Q} }(s_1, a)$ towards $r + \gamma \mathrm{\hat{V} }(s_2)$.

    The loss function for these updates is the mean squared error.

    We also apply a sample of the policy gradient to the actor. For one
    transition in the experience buffer, our sample is
    $(\mathrm{\hat{Q} }(s_1, a) - \mathrm{\hat{V} }(s_1)) \cdot
    \nabla_\theta \log \pi_\theta(s_1,a)$.

    We aggregate this update over the whole experience buffer. This is,
    strictly speaking, not mathematically correct, because the policy changes
    on every step. While the latest step is sampled from the current policy,
    older samples in the experience buffer have been sampled from an older
    policy. So we are missing an importance sampling factor of
    $\frac{\pi'(s,a)}{\pi(s,a)}$. But hopefully this is "no big deal".

    I compute the policy gradient over the whole replay buffer because I hope
    this would give me lower variance on the policy gradient samples, so
    hopefully faster convergence.

    If I needed to fix this, I could keep the probabilities (i.e., values of
    $\pi(s,a)$ at time of sampling) in the experience buffer to compute the
    importance sampling ratios. Or keep the policy frozen until the experience
    buffer is filled up, then use it to compute one update, then flush it.
    """
    def __init__(
        self,
        observation_space,
        action_space,
        actor_layer_sizes: Sequence[int],
        actor_learning_rate,
        critic_layer_sizes: Sequence[int],
        critic_learning_rate,
        replay_buffer_size: int,
        discount_rate,
        critic_sync_probability,
        log_weights: bool,
        log_value_histograms: bool,
        log_advantage: bool,
    ):
        # TODO: DDPG has not just target & optimized value networks, but also
        # target & optimized policy networks. Maybe split policy networks?
        # TODO: DDPG doesn't have switching of networks, but gradual update
        # towards the optimized net.

        # Action space should be discrete.
        assert isinstance(action_space, gym.spaces.Discrete)

        # Observation space should be a 1D box of floats.
        assert isinstance(observation_space, gym.spaces.Box)
        assert observation_space.dtype.name == 'float32'
        assert len(observation_space.shape) == 1

        self._action_space = action_space
        self._observation_space = observation_space
        self._actor = _make_actor(observation_space, action_space,
                                  actor_layer_sizes)
        self._critic_optimized = _make_critic(observation_space, action_space,
                                              critic_layer_sizes)
        self._critic_target = _make_critic(observation_space, action_space,
                                           critic_layer_sizes)
        self._last_observation = tf.Variable(tf.zeros(observation_space.shape),
                                             shape=observation_space.shape,
                                             dtype=tf.float32,
                                             name='last_observation')
        self._last_action = tf.Variable(0,
                                        shape=(),
                                        dtype=tf.int32,
                                        name='last_action')
        self._discount_rate = tf.constant(discount_rate, dtype=tf.float32)
        self._actor_optimizer = tf.keras.optimizers.Adam(
            learning_rate=actor_learning_rate)
        self._critic_optimizer = tf.keras.optimizers.Adam(
            learning_rate=critic_learning_rate)
        self._critic_sync_probability = critic_sync_probability

        # Replay buffer: S, A, R, S'
        self._replay_buffer = ReplayBuffer(
            capacity=replay_buffer_size,
            observation_space=observation_space,
            action_space=action_space,
        )

        self._should_log_weights = log_weights
        self._should_log_value_histograms = log_value_histograms
        self._should_log_advantage = log_advantage

    @tf.function
    def _sample_action(self, observation, exploration_rate):
        observation_input = tf.expand_dims(observation, 0)
        actor_dist = self._actor({
            'observation':
            observation_input,
            'exploration_rate':
            tf.expand_dims(exploration_rate, 0),
        })
        sample = actor_dist.sample()

        action_probs = actor_dist.prob(
            tf.range(self._action_space.n, dtype=tf.float32))
        metrics = {}
        for i in range(self._action_space.n):
            metrics[f'action_prob/{i}'] = action_probs[i]
        metrics['action_prob/min'] = tf.reduce_min(action_probs)

        return sample[0], metrics

    def start(self, observation, exploration_rate):
        action, metrics = self._sample_action(observation, exploration_rate)
        self._last_action.assign(action)
        self._last_observation.assign(observation)
        return action.numpy(), metrics

    @tf.function
    def step(self, reward, observation, exploration_rate, learn=True):
        metrics = {}

        if learn:
            reward = tf.cast(reward, tf.float32)
            exploration_rate = tf.cast(exploration_rate, tf.float32)

            self._replay_buffer.store(
                prestate=self._last_observation,
                action=self._last_action,
                reward=reward,
                poststate=observation,
                done=False,
            )

            # Update critic to better model V(s1), Q(s1, a1).
            metrics.update(
                self._critic_update(exploration_rate=exploration_rate))
            # Update actor to according to Q(s1, a1) - V(s1).
            metrics.update(
                self._actor_update(exploration_rate=exploration_rate))

        action, metrics2 = self._sample_action(observation, exploration_rate)
        metrics.update(metrics2)

        self._last_action.assign(action)
        self._last_observation.assign(observation)

        if learn:
            self._maybe_sync_critics()
        self._log_weights()

        return action, metrics

    def _log_model_weights(self, model, name):
        for variable in model.trainable_variables:
            tf.summary.histogram(f'{name}/{variable.name}',
                                 variable.read_value())

    def _log_weights(self):
        if not self._should_log_weights:
            return

        self._log_model_weights(self._actor, 'actor')
        self._log_model_weights(self._critic_optimized, 'critic_optimized')

    def finish(self, reward, exploration_rate, learn=True):
        if not learn:
            return {}

        reward = tf.constant(reward, tf.float32)
        exploration_rate = tf.constant(exploration_rate, tf.float32)

        self._replay_buffer.store(
            self._last_observation,
            self._last_action,
            reward=reward,
            poststate=tf.zeros(self._observation_space.shape,
                               dtype=tf.float32),
            done=True,
        )

        # Update critic to better model V(s1), Q(s1, a1).
        metrics = self._critic_update(exploration_rate=exploration_rate)
        # Update actor to according to Q(s1, a1) - V(s1).
        metrics.update(self._actor_update(exploration_rate=exploration_rate))

        self._maybe_sync_critics()
        return metrics

    def _sync_critics(self):
        for target_var, optimized_var in zip(
                self._critic_target.trainable_variables,
                self._critic_optimized.trainable_variables):
            target_var.assign(optimized_var.read_value())

    def _maybe_sync_critics(self):
        if tf.random.uniform(()) < self._critic_sync_probability:
            self._sync_critics()

    @tf.function
    def _critic_update(self, exploration_rate):
        """Update critic to better model V(prestate) and Q(prestate, action)."""
        poststate_critic_output = self._critic_target(
            self._replay_buffer._poststates)
        poststate_values = poststate_critic_output['state_value'][:, 0]
        poststate_values = tf.where(
            self._replay_buffer._done,
            tf.zeros_like(poststate_values, tf.float32),
            poststate_values,
        )
        action_value_sample = (self._replay_buffer._rewards +
                               self._discount_rate * poststate_values)
        # Expected SARSA update
        actions = tf.range(self._action_space.n, dtype=tf.float32)
        actions_dist = self._actor({
            'observation':
            self._replay_buffer._prestates,
            'exploration_rate': (tf.ones(
                (self._replay_buffer._capacity, 1)) * exploration_rate),
        })
        actions_prob = actions_dist.prob(
            tf.broadcast_to(
                tf.expand_dims(actions, 1),
                (self._action_space.n, self._replay_buffer._capacity),
            ))
        poststate_value_sample = tf.reduce_sum(
            tf.transpose(actions_prob) *
            poststate_critic_output['action_values'],
            axis=1,
        )
        poststate_value_sample = tf.where(
            self._replay_buffer._done,
            tf.zeros_like(poststate_value_sample, tf.float32),
            poststate_value_sample,
        )
        prestate_value_sample = (self._replay_buffer._rewards +
                                 self._discount_rate * poststate_value_sample)

        if self._should_log_value_histograms:
            tf.summary.histogram('critic_target/action_value',
                                 action_value_sample)
            tf.summary.histogram('critic_target/state_value',
                                 prestate_value_sample)

        with tf.GradientTape() as g:
            critic_output = self._critic_optimized(
                self._replay_buffer._prestates)
            indices = tf.stack([
                tf.range(self._replay_buffer._capacity),
                self._replay_buffer._actions,
            ],
                               axis=1)
            predicted_action_values = tf.gather_nd(
                critic_output['action_values'],
                indices,
            )
            predicted_prestate_value = critic_output['state_value'][:, 0]

            action_value_loss = tf.math.pow(
                predicted_action_values - action_value_sample, 2)
            state_value_loss = tf.math.pow(
                predicted_prestate_value - prestate_value_sample, 2)

            action_value_loss = tf.where(
                self._replay_buffer._used,
                action_value_loss,
                tf.zeros_like(action_value_loss, tf.float32),
            )
            state_value_loss = tf.where(
                self._replay_buffer._used,
                state_value_loss,
                tf.zeros_like(state_value_loss, tf.float32),
            )

            n = tf.cast(self._replay_buffer._count, tf.float32)
            tf.debugging.assert_positive(n, "nothing in replay buffer")
            action_value_loss = tf.reduce_sum(action_value_loss / n)
            state_value_loss = tf.reduce_sum(state_value_loss / n)

            loss = action_value_loss + state_value_loss

        if self._should_log_value_histograms:
            tf.summary.histogram('critic_prediction/action_value',
                                 predicted_action_values)
            tf.summary.histogram('critic_prediction/prestate_value',
                                 predicted_prestate_value)

        critic_gradients = g.gradient(
            loss, self._critic_optimized.trainable_variables)
        self._critic_optimizer.apply_gradients(
            zip(critic_gradients, self._critic_optimized.trainable_variables))

        metrics = {
            'critic_loss/action_value': action_value_loss,
            'critic_loss/state_value': state_value_loss,
            'critic_loss/total': loss,
            'critic_lr': self._get_critic_optimizer_learning_rate(),
        }
        return metrics

    def _get_critic_optimizer_learning_rate(self):
        if isinstance(self._critic_optimizer.learning_rate,
                      tf.keras.optimizers.schedules.LearningRateSchedule):
            return self._critic_optimizer.learning_rate(
                self._critic_optimizer.iterations)
        else:
            return self._critic_optimizer.learning_rate

    @tf.function
    def _actor_update(self, exploration_rate):
        critic_output = self._critic_target(self._replay_buffer._prestates)
        indices = tf.stack([
            tf.range(self._replay_buffer._capacity),
            self._replay_buffer._actions,
        ],
                           axis=1)
        action_values = tf.gather_nd(critic_output['action_values'], indices)
        state_values = critic_output['state_value'][:, 0]
        advantage = action_values - state_values
        if self._should_log_advantage:
            tf.summary.histogram('advantage', advantage)

        with tf.GradientTape() as g:
            actor_dist = self._actor({
                'observation':
                self._replay_buffer._prestates,
                'exploration_rate': (tf.ones(
                    (self._replay_buffer._capacity, 1)) * exploration_rate),
            })
            log_prob = tf.where(
                self._replay_buffer._used,
                actor_dist.log_prob(self._replay_buffer._actions),
                0,
            )
            actor_pseudo_loss = -tf.reduce_sum(log_prob * advantage)

        actor_gradient = g.gradient(actor_pseudo_loss,
                                    self._actor.trainable_variables)

        self._actor_optimizer.apply_gradients(
            zip(actor_gradient, self._actor.trainable_variables))

        metrics = {
            'actor_pseudo_loss': actor_pseudo_loss,
        }
        return metrics


exp = sacred.Experiment('lunar-lander-ac', interactive=True)


@exp.config
def config():
    environment = 'LunarLander-v2'
    log_dir = None
    critic_learning_rate = 1e-4  # Critic LR
    actor_layer_sizes = [128, 32]  # Actor layers
    critic_layer_sizes = [128, 32]  # Critic layers
    actor_learning_rate = 1e-6  # Actor LR
    replay_buffer_size = 2**11  # Replay buffer size
    discount_rate = 1.0  # Discount rate
    critic_sync_probability = 1e-3  # Critic sync probability
    exploration_rate_min = 0.1  # Exploration rate min.
    exploration_rate_decay = 0.0  # Exploration rate decay
    sacred_scalar_logging_episode_frequency = 20
    log_scalars = True
    log_weights = False
    log_value_histograms = False
    log_advantage = False
    record_video = True
    # Number of episodes to run. If <= 0, run forever.
    num_episodes = 0
    save_weights_freq_minutes = 10


@exp.automain
def run(
    _run,
    _seed,
    _log,
    environment,
    log_dir,
    actor_layer_sizes,
    critic_layer_sizes,
    actor_learning_rate,
    critic_learning_rate,
    replay_buffer_size,
    discount_rate,
    critic_sync_probability,
    exploration_rate_min,
    exploration_rate_decay,
    log_scalars: bool,
    log_weights: bool,
    log_value_histograms: bool,
    log_advantage: bool,
    record_video: bool,
    num_episodes: int,
    sacred_scalar_logging_episode_frequency: int,
    save_weights_freq_minutes: int,
    callbacks: Sequence[callbacks_lib.Callback] = None,
):
    _log.info("***************************************************")
    if log_dir is None:
        log_dir = f'/tmp/lunar-lander-ac/{datetime.datetime.now().isoformat()}'
        _log.info(f"log_dir not specified, picking {log_dir}")

    # TODO: would be nice to also launch TensorBoard and open it ourselves
    _log.info(f"To follow along in TensorBoard, run:")
    _log.info(f"   tensorboard --logdir {log_dir}")
    _log.info("***************************************************")

    # TODO: Sacred's LogFileWriter ought to populate this. See
    # https://github.com/IDSIA/sacred/issues/836.
    exp.info['tensorflow'] = {'log_dir': log_dir}

    env = gym.make(environment)
    if record_video:
        env = monitor.Monitor(env, log_dir, force=True)
    env.seed(_seed)

    episode = 0

    if callbacks is None:
        callbacks = [callbacks_lib.TrainingTQDMCallback()]

    # TODO: make this callback independent
    if save_weights_freq_minutes > 0:
        callbacks.append(
            callbacks_lib.SaveWeightsCallback(
                freq=datetime.timedelta(minutes=save_weights_freq_minutes),
                _log=_log,
                _run=_run,
                log_dir=log_dir,
            ))
    if record_video:
        callbacks.append(callbacks_lib.SaveVideoCallback(env=env, _run=_run))

    # TODO: move to callback
    def report_metrics(metrics):
        if not log_scalars:
            return

        for metric, value in metrics.items():
            tf.summary.scalar(metric, value)
            if isinstance(value, tf.Tensor):
                value = value.numpy()

            if episode % sacred_scalar_logging_episode_frequency == 0:
                _run.log_scalar(metric, value)

    agent = ActorCriticAgent(
        observation_space=env.observation_space,
        action_space=env.action_space,
        actor_layer_sizes=actor_layer_sizes,
        critic_layer_sizes=critic_layer_sizes,
        actor_learning_rate=actor_learning_rate,
        critic_learning_rate=critic_learning_rate,
        replay_buffer_size=replay_buffer_size,
        discount_rate=discount_rate,
        critic_sync_probability=critic_sync_probability,
        log_weights=log_weights,
        log_value_histograms=log_value_histograms,
        log_advantage=log_advantage,
    )

    if num_episodes <= 0:
        num_episodes = None

    with tf.summary.create_file_writer(log_dir).as_default():
        scaffold_lib.run_episodes(
            env=env,
            agent=agent,
            num_episodes=num_episodes,
            callbacks=callbacks,
            report_metrics=report_metrics,
            exploration_rate_schedule=lambda episode:
            (exploration_rate_min + (1.0 - exploration_rate_min) *
             (exploration_rate_decay**(episode + 1))),
        )


# possible: env.render(close=True)
# print(env.observation_space.high); also .low
# (or env.render(mode='close') ?)
