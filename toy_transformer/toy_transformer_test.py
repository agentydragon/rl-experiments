import torch

from absl.testing import absltest
from rl_experiments.toy_transformer.toy_transformer import MultiHeadAttention, TransformerLayer


class MultiHeadAttentionTest(absltest.TestCase):

    def test_shapes(self):
        input_dim = 8
        output_dim = 4
        batch_size = 16
        sequence_length = 10
        key_dim = 6
        value_dim = 6

        x = torch.randn((batch_size, sequence_length, input_dim))
        h = MultiHeadAttention(
            input_dim=input_dim,
            output_dim=output_dim,
            num_heads=3,
            key_dim=key_dim,
            value_dim=value_dim,
        )
        assert h(x).shape == (batch_size, sequence_length, output_dim)


class TransformerLayerTest(absltest.TestCase):

    def test_shapes(self):
        batch_size = 16
        sequence_length = 10
        embedding_dim = 8
        key_dim = 4
        value_dim = 6
        feedforward_dim = 7

        x = torch.randn((batch_size, sequence_length, embedding_dim))

        h = TransformerLayer(
            sequence_size=sequence_length,
            num_heads=3,
            embedding_dim=embedding_dim,
            key_dim=key_dim,
            value_dim=value_dim,
            feedforward_dim=feedforward_dim,
        )
        assert h(x).shape == (batch_size, sequence_length, embedding_dim)


if __name__ == '__main__':
    absltest.main()
