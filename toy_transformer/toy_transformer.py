import torch
from torch import nn
from torch.nn import functional as F

import random


class OneHeadAttention(nn.Module):

    def __init__(self, *, input_dim, output_dim):
        super().__init__()
        self.q = nn.Parameter(torch.randn((input_dim, output_dim)))
        self.k = nn.Parameter(torch.randn((input_dim, output_dim)))
        self.v = nn.Parameter(torch.randn((input_dim, output_dim)))

    def forward(self, x):
        queries = torch.tensordot(x, self.q, dims=([2], [0]))
        keys = torch.tensordot(x, self.k, dims=([2], [0]))
        values = torch.tensordot(x, self.v, dims=([2], [0]))
        keys = keys.transpose(2, 1)
        attention = torch.matmul(queries, keys)
        attention = attention.softmax(dim=2)
        y = torch.matmul(attention, values)
        return y


class MultiHeadAttention(nn.Module):

    def __init__(
        self,
        *,
        num_heads: int,
        input_dim: int,
        output_dim: int,
        key_dim: int,
        value_dim: int,
    ):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.num_heads = num_heads
        self.key_dim = key_dim
        self.value_dim = value_dim

        # TODO: this might not be great initialization...
        self.q = nn.Parameter(torch.randn((input_dim, num_heads * key_dim)))
        self.k = nn.Parameter(torch.randn((input_dim, num_heads * key_dim)))
        self.v = nn.Parameter(torch.randn((input_dim, num_heads * value_dim)))
        self.agg = nn.Parameter(
            torch.randn((num_heads * value_dim, output_dim)))

    def forward(self, x):
        # x.shape: [batch, sequence_size, input_dim]
        # Compute queries
        #print("x:", x.shape)
        queries = torch.tensordot(x, self.q, dims=([2], [0]))
        keys = torch.tensordot(x, self.k, dims=([2], [0]))
        values = torch.tensordot(x, self.v, dims=([2], [0]))

        # queries, keys, values shape == [batch, seq, num_heads * output_dim]

        batch = queries.shape[0]
        seq = queries.shape[1]
        queries = queries.reshape([batch, seq, self.num_heads,
                                   self.key_dim]).transpose(1, 2)
        keys = keys.reshape([batch, seq, self.num_heads,
                             self.key_dim]).transpose(1, 2)
        values = values.reshape([batch, seq, self.num_heads,
                                 self.value_dim]).transpose(1, 2)

        keys = keys.transpose(2, 3)

        # queries shape == [batch, head, seq, key_dim]
        # keys shape == [batch, head, key_dim, seq]
        # values shape == [batch, head, seq, value_dim]

        attention = torch.matmul(queries, keys)
        # attention.shape == [batch, head, seq, seq]
        attention = (attention /
                     torch.sqrt(torch.tensor(self.key_dim))).softmax(dim=3)

        #print("attention:", attention.shape)  # [batch_size, sequence length, sequence length]
        ys = torch.matmul(attention, values)
        # ys shape == [batch, head, seq, value_dim]
        ys = ys.transpose(1, 2)
        # ys shape == [batch, seq, head, value_dim]
        ys = ys.reshape([batch, seq, self.num_heads * self.value_dim])
        # ys shape == [batch, seq, head * value_dim]
        ys = torch.tensordot(ys, self.agg, dims=([2], [0]))
        # ys shape == [batch, seq, output_dim]

        assert ys.shape[2] == self.output_dim

        return ys


N = 256  # each byte


def make_position_encoding(*, sequence_size, dims):
    # each word needs to have its own specific sequence encoding, and the encoding
    # needs to change predictably over time. so you have to be able to reason over
    # the encoding.

    # each sine/cosine will be on 50% of the time. they will work kinda like bits.
    # 2^num_frequencies >= sequence_size, otherwise we couldn't distinguish between words
    # on the scale.
    # sequence_size <= 2 ^ num_frequencies

    # create 'dims' rows, each increasing by 1. get power of 2 that corresponds.
    # frequencies have to form a geometric progression from 2pi to 10 000 * 2pi over
    # the wavelengths.
    # omega_k = 1 / 10000^(2k / d)
    #    omega = torch.reciprocal(torch.pow(10_000.0, torch.range(dims) * 2 / dims))
    #    angles =
    # contain a pair of sines and cosines with each frequency.
    assert dims % 2 == 0

    # shape: [dims / 2]
    omega = torch.logspace(0, -1, steps=(dims // 2), base=10_000)
    # shape: [sequence_size]
    time = torch.arange(sequence_size)

    # shape: [sequence_size, dims / 2]
    angle = omega.unsqueeze(0) * time.unsqueeze(1)

    #print(angle.shape)
    assert angle.shape == (sequence_size, dims // 2)

    encodings = torch.cat([
        torch.sin(angle),
        torch.cos(angle),
    ], dim=1)

    assert encodings.shape == (sequence_size, dims)

    return encodings


class TransformerLayer(nn.Module):

    def __init__(
        self,
        *,
        sequence_size: int,
        num_heads: int,
        embedding_dim: int,
        key_dim: int,
        value_dim: int,
        feedforward_dim: int,
    ):
        super().__init__()

        self.attention = MultiHeadAttention(
            input_dim=embedding_dim,
            key_dim=key_dim,
            value_dim=value_dim,
            output_dim=embedding_dim,
            num_heads=num_heads,
        )
        self.linear1 = nn.Linear(
            in_features=embedding_dim,
            out_features=feedforward_dim,
        )
        self.linear2 = nn.Linear(
            in_features=feedforward_dim,
            out_features=embedding_dim,
        )
        self.sequence_size = sequence_size
        self.embedding_dim = embedding_dim

    def forward(self, x):
        # normalized_dims = (embedding_dim,)
        normalized_dims = (self.sequence_size, self.embedding_dim)
        x = F.layer_norm(self.attention(x) + x, normalized_dims)
        x = F.layer_norm(
            self.linear2(F.relu(self.linear1(x))) + x, normalized_dims)
        return x


class SimpleTransformer(nn.Module):

    def __init__(
        self,
        *,
        sequence_size: int,
        num_heads: int,
        layers: int,
        embedding_dim: int,
        position_encoding_dims: int,
        feedforward_dim: int,
        key_dim: int,
        value_dim: int,
        num_embeddings: int,
    ):
        super().__init__()
        assert embedding_dim > position_encoding_dims
        initial_embedding_dim = embedding_dim - position_encoding_dims

        self.sequence_size = sequence_size

        # Initial embeddings.
        self.embedding = nn.Embedding(
            num_embeddings=num_embeddings,
            embedding_dim=initial_embedding_dim,
        )
        self.position_encoding = nn.Parameter(
            make_position_encoding(
                sequence_size=self.sequence_size,
                dims=position_encoding_dims,
            ),
            requires_grad=False,
        )
        """
        encoding desiderata:
        
        Ideally, the following criteria should be satisfied:

        - It should output a unique encoding for each time-step (word’s position in a sentence)
        - Distance between any two time-steps should be consistent across sentences with different lengths.
        - Our model should generalize to longer sentences without any efforts. Its values should be bounded.
        - It must be deterministic.
        """

        self.layers = nn.ModuleList([
            TransformerLayer(
                sequence_size=sequence_size,
                num_heads=num_heads,
                embedding_dim=embedding_dim,
                feedforward_dim=feedforward_dim,
                key_dim=key_dim,
                value_dim=value_dim,
            ) for _ in range(layers)
        ])

        self.final_linear = nn.Linear(
            in_features=embedding_dim,
            out_features=N,
        )

        # input: [batch, sequence length, embedding dimension]
        # need to normalize over sequences

        #self.stack = nn.Sequential(*modules)
        # TODO: make sure that the position encoding actually does not get updated

        # TODO: residual connections

    def forward(self, x):
        # x.shape = [batch size, sequence size]
        # add position encoding
        position_encoding = self.position_encoding
        # position encoding shape: [sequence_size, position_encoding_dims]
        position_encoding = position_encoding.unsqueeze(0).broadcast_to(
            (x.shape[0], x.shape[1], -1))

        x = self.embedding(x)
        x = torch.concat([x, position_encoding], dim=-1)

        for layer in self.layers:
            x = layer(x)

        # shape: [batch, sequence size, encoding]
        x = x.mean(dim=1)

        # shape: [batch, encoding]
        x = self.final_linear(x)

        # x = self.flatten(x)
        # print("x pre-linear:", x)
        # x = self.final_linear(x)
        # print("final x:", x)
        return x


def bytes_to_tensor(x):
    return torch.tensor([int(byte) for byte in x], dtype=torch.int)


class TextDataset:

    def __init__(self, *, text, sequence_size):
        self.text = text
        self.sequence_size = sequence_size

    def __getitem__(self, i):
        line = self.text[i:i + self.sequence_size + 1]
        return (bytes_to_tensor(line[:-1]), torch.tensor(line[-1]))

    def __len__(self):
        return len(self.text) - self.sequence_size - 1


# TODO: this dataset puts *way* too much data into memory.
# I don't actually need to initialize the whole array!
def dataset_from_file(path, *, sequence_size, max_size=None):
    with open(path, 'rb') as f:
        s = f.read()

    return TextDataset(text=s, sequence_size=sequence_size)


def sample(t, prompt: bytes, length: int = 200):
    assert len(prompt) >= t.sequence_size
    continuation = b''
    # len = 83

    for _ in range(length):
        b = (prompt + continuation)[-t.sequence_size:]
        assert len(b) == t.sequence_size
        b = bytes_to_tensor(b).unsqueeze(0).to('cuda')
        scores = t(b).squeeze(0)
        dist = torch.distributions.categorical.Categorical(logits=scores)
        continuation += bytes([dist.sample().item()])

    return prompt.decode('utf-8', errors='ignore') + '|' + continuation.decode(
        'utf-8', errors='ignore')


def make_simple_dataset():
    # simple example
    dataset = []
    for i in range(10):
        line = ""
        for x in range(SEQUENCE_SIZE + 1):
            line += str((x + i) % 10)
        dataset.append((line[:-1], line[-1]))

    dataset = [(torch.tensor([byte for byte in x.encode('utf-8')]),
                torch.tensor(y.encode('utf-8')[0])) for x, y in dataset]

    return dataset
