url = "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"  #wget -w 2 -m -H "http://www.gutenberg.org/robot/harvest?filetypes[]=html&langs[]=en"

import requests
# pip install beautifulsoup4
from bs4 import BeautifulSoup
import io
import zipfile
import time

with open('pg.txt', 'wb') as outfile:
    content = requests.get(url).content
    print(content)

    soup = BeautifulSoup(content, "html.parser")

    # *** START OF THIS PROJECT GUTENBERG EBOOK PUNCH ***
    # *** END OF THIS PROJECT GUTENBERG EBOOK PUNCH ***

    for a in soup.find_all('a'):
        href = a.attrs['href']
        if '.zip' in href:
            print(href)

            resp = requests.get(href).content
            i = io.BytesIO(resp)
            with zipfile.ZipFile(i) as f:

                # unzip all files that have .txt in name

                for info in f.infolist():
                    print(info)
                    if info.filename.endswith('.txt'):
                        # print("YES, download this")
                        # print(f.read(info.filename))
                        with f.open(info.filename, 'r') as textfile:
                            stage = 'not_started'
                            for line in textfile:
                                if stage == 'not_started' and (
                                        b'*** START OF THIS PROJECT GUTENBERG EBOOK'
                                        in line):
                                    stage = 'started'
                                elif stage == 'started':
                                    if (b'*** END OF THIS PROJECT GUTENBERG EBOOK'
                                            in line):
                                        stage = 'ended'
                                    else:
                                        outfile.write(line)
                        if stage != 'ended':
                            print('not ended:', stage, info.filename)
                            outfile.write(f.read(info.filename))
            # harvest?offset=40538&amp;filetypes[]=txt&amp;langs[]=en

        else:
            # TODO
            pass
        #time.sleep(10)
        time.sleep(2)
